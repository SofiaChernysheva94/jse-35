package ru.t1.chernysheva.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.response.data.*;
import ru.t1.chernysheva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndPoint {


    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }


    @NotNull
    DataLoadBackupResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBackupRequest request
    ) throws AbstractException;

    @NotNull
    DataLoadBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBase64Request request
    ) throws AbstractException;

    @NotNull
    DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBinaryRequest request
    ) throws AbstractException;

    @NotNull
    DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    DataLoadJsonJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonJaxBRequest request
    ) throws AbstractException;

    @NotNull
    DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    DataLoadXmlJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlJaxBRequest request
    ) throws AbstractException;

    @NotNull
    DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadYamlFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveBackupResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBackupRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBase64Request request
    ) throws AbstractException;

    @NotNull
    DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBinaryRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveJsonJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonJaxBRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveXmlJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlJaxBRequest request
    ) throws AbstractException;

    @NotNull
    DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveYamlFasterXmlRequest request
    ) throws AbstractException;

}
