package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListResponse extends AbstractUserResponse {

    private List<Task> tasks;

    public TaskListResponse(List<Task> tasks) {
        this.tasks = tasks;
    }


}
