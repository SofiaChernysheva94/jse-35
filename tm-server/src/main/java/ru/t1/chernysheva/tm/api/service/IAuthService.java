package ru.t1.chernysheva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.Session;
import ru.t1.chernysheva.tm.model.User;

public interface IAuthService {

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    String login(@Nullable String login, @Nullable String password);

}
