package ru.t1.chernysheva.tm.repository;

import ru.t1.chernysheva.tm.api.repository.ISessionRepository;
import ru.t1.chernysheva.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
