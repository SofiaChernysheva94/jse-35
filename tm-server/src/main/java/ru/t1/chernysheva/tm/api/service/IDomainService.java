package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.response.data.*;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void loadDataJsonJaxB();

    void saveDataJsonFasterXml();

    void saveDataJsonJaxB();

    void loadDataXmlFasterXml();

    void loadDataXmlJaxB();

    void saveDataXmlFasterXml();

    void saveDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}
