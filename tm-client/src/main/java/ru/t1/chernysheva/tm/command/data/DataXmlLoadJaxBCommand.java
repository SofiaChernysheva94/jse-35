package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadXmlFasterXmlRequest;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadXmlJaxBRequest;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION  = "Load data from xml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataLoadXmlJaxBRequest request = new DataLoadXmlJaxBRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
