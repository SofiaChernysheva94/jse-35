package ru.t1.chernysheva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-complete-by-id";

    @NotNull
    private final String DESCRIPTION = "Complete task by Id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);

        getTaskEndpoint().completeTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
