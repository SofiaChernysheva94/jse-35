package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.project.ProjectCompleteByIdRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-complete-by-id";

    @NotNull
    private final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken());
        request.setId(id);

        getProjectEndpoint().completeProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
